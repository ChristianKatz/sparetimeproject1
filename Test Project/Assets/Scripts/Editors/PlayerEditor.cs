﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*[CustomEditor(typeof(MyPlayer))]
[CanEditMultipleObjects]
public class PlayerEditor : Editor
{
    SerializedProperty damageProperty;
    SerializedProperty armorProperty;
    SerializedProperty HealthProperty;
    SerializedProperty SpeedProperty;

    private void OnEnable()
    {
        damageProperty = serializedObject.FindProperty("Damage");
        armorProperty = serializedObject.FindProperty("Armor");
        HealthProperty = serializedObject.FindProperty("Health");
        SpeedProperty = serializedObject.FindProperty("speed");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (!damageProperty.hasMultipleDifferentValues)
            ProgessBar(damageProperty.intValue / 100.0f, "Damage Value: " + damageProperty.intValue);
        EditorGUILayout.IntSlider(damageProperty, 0, 100, new GUIContent("Damage"));

        if (!armorProperty.hasMultipleDifferentValues)
            ProgessBar(armorProperty.intValue / 100.0f, "Armor Value: " + armorProperty.intValue);
        EditorGUILayout.IntSlider(armorProperty, 0, 100, new GUIContent("Armor"));

        if (!HealthProperty.hasMultipleDifferentValues)
            ProgessBar(HealthProperty.intValue / 100.0f, "Health Value: " + HealthProperty.intValue);
        EditorGUILayout.IntSlider(HealthProperty, 0, 100, new GUIContent("Health"));

        if (!SpeedProperty.hasMultipleDifferentValues)
            ProgessBar(SpeedProperty.intValue / 100.0f, "Speed Value: " + SpeedProperty.intValue);
        EditorGUILayout.IntSlider(SpeedProperty, 0, 100, new GUIContent("Speed"));

        

        serializedObject.ApplyModifiedProperties();
    }
    public void ProgessBar(float value, string label)
    {
        Rect rect = GUILayoutUtility.GetRect(18, 18, "Textfield");
        EditorGUI.ProgressBar(rect, value, label);
        EditorGUILayout.Space();
    }
}

    */