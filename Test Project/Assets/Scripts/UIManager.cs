﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private float PowerChargeLaser;
    [SerializeField]
    private Slider LaserSlider;

    private float powerChargerRocket;
    public float PowerChargeRocket
    {
        get
        {
        return powerChargerRocket;
        }
        set
        {
        powerChargerRocket = value;
        }
    }
    [SerializeField]
    private Slider RocketSlider;
    [SerializeField]
    private TextMeshProUGUI chargeRocket;
    private Rocket rocketScript;


    [SerializeField]
    private TextMeshProUGUI chargeMortar;    
    [SerializeField]
    private Slider PowerCharge;
    [SerializeField]
    private TextMeshProUGUI CooldownText;
    private float Cooldown = 5;
    
    private MyPlayer PlayerScricpt;

    void Start()
    {
        PlayerScricpt = FindObjectOfType<MyPlayer>();
        rocketScript = FindObjectOfType<Rocket>();
    }

    void Update()
    {
        chargePowerOfMortar();
        ChargePowerOfLaser();
        ChargePowerOfRocket();
    }
      void chargePowerOfMortar()
      {
        
        PowerCharge.value = PlayerScricpt.ShootPower;
        PlayerScricpt.ShootPower = Mathf.Round(PlayerScricpt.ShootPower);
        chargeMortar.text = string.Format(PlayerScricpt.ShootPower  / 400 * 10 + " %");

        if (PlayerScricpt.GranateFired == true)
        {
            Cooldown = Cooldown - Time.deltaTime;           
            CooldownText.text = string.Format("Cooldown: {0:0} ", Cooldown);
            if (Cooldown <= 0)
            {
                PlayerScricpt.GranateFired = false;
                Cooldown = 5;          
            }
        }       
      }

     void ChargePowerOfLaser()
     {
        if(PlayerScricpt.GunFired == true)
        {
            PowerChargeLaser += Time.deltaTime * 100;
            LaserSlider.value = PowerChargeLaser;                    
        }
        if (PlayerScricpt.GunFired == false)
        {
            PowerChargeLaser = 0;
            LaserSlider.value = PowerChargeLaser;         
        }
     }

    void ChargePowerOfRocket()
    {
        if(rocketScript.PressButton == true)
        {
        powerChargerRocket += Time.deltaTime * 10;
        RocketSlider.value = powerChargerRocket;
            chargeRocket.text = string.Format(powerChargerRocket + " %");        
            if (powerChargerRocket >= 100)
                powerChargerRocket = 100;
        }

    }


}
