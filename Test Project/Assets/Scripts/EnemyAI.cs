﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{

    
    [SerializeField]
    private Transform Target;
    private NavMeshAgent EnemyAgent;
    private Rigidbody rb;
    private float moveVertical = 10;
    private float moveHorizontal;
    [SerializeField]
    private Transform jumpLocation;
    private bool isGrounded;
    private float maxJumpRange = 0.1f;
    [SerializeField]
    private LayerMask GroundLayer;

    [SerializeField]
    private Transform ShootPosition;
    [SerializeField]
    private int fireRange;
    [SerializeField]
    private float fireRate;
    private float fireBreak;
    private LineRenderer Laser;
    [SerializeField]
    private int Radius;

    private int MovementDecision;

    void Start()
    {        
        EnemyAgent = GetComponent<NavMeshAgent>();
        ShootPosition.transform.Find("GunRight (1)");
        Laser = GetComponent<LineRenderer>();
        Laser.enabled = false;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {      
        searchTarget();
    }

    private void Movement()
    {
        StartCoroutine(MovementBreak());

        if(MovementDecision == 1)
        {
            isGrounded = Physics.Raycast(jumpLocation.position, jumpLocation.TransformDirection(Vector3.down), maxJumpRange, GroundLayer);
            if (isGrounded == true)
            rb.AddForce(0, 500, 0);
        }
        if(MovementDecision == 2)
        {
            rb.MovePosition(transform.position + transform.TransformDirection(new Vector3(moveVertical, 0, 0)));
        }
        if (MovementDecision == 3)
        {
            rb.MovePosition(transform.position + transform.TransformDirection(new Vector3(-moveVertical, 0, 0)));
        }

        rb.MovePosition(transform.position + transform.TransformDirection(new Vector3(0, 0, 0)));

        IEnumerator MovementBreak()
        {
            yield return new WaitForSeconds(5f);
            MovementDecision = Random.Range(1, 3);
            yield return new WaitForSeconds(5f);
            StartCoroutine(MovementBreak());
        }
    }

    private void ShootingWithGun()
    {
      if (Time.time > fireBreak)
      {
         fireBreak = Time.time + fireRate;        
         Laser.SetPosition(0, ShootPosition.position);
            StartCoroutine(ShootEffect());
            RaycastHit hit;
            if(Physics.Raycast(ShootPosition.position, ShootPosition.transform.forward, out hit, fireRange))
            {                
                
                Laser.SetPosition(1, hit.point);

                if (hit.rigidbody != null)
                    hit.rigidbody.AddForce(-hit.normal * 300);
            }           

      }
      IEnumerator ShootEffect()
      {
            Laser.enabled = true;
            yield return new WaitForSeconds(0.2f);
            Laser.enabled = false;
      }

    }

    private void FaceTarget()
    {                            
        Vector3 direction = (transform.position - Target.position).normalized;       
        Quaternion RotationToTarget = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, RotationToTarget, Time.deltaTime * 10);      
    }

    private void searchTarget()
    {
        float Distance = Vector3.Distance(transform.position,Target.position);

        if(Distance <= Radius)
        {
            FaceTarget();
            ShootingWithGun();
            Movement();
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Radius);
            
    }

}
