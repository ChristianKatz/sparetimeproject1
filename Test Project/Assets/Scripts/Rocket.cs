﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Rocket : MonoBehaviour
{
    private Rigidbody RocketRb;
    [SerializeField]
    private GameObject RocketGameobject;
    [SerializeField]
    private int RocketForce;
    private Transform Target;
    private float cooldown = 3;

    [SerializeField]
    private  Transform GizmosCenter;
    [SerializeField]
    private float GizmosRadius;
    [SerializeField]
    private Transform InteraktionPoint;
    [SerializeField]
    private TextMeshProUGUI PressF;

    private UIManager uIManager;
    [SerializeField]
    private Transform Player;


    private bool pressButton = false;
    public bool PressButton
    {
        get
        {
        return pressButton;
        }
        set
        {
        pressButton = value;
        }
    }


// Start is called before the first frame update
    void Start()
    {
        RocketRb = GetComponent<Rigidbody>();
        uIManager = FindObjectOfType<UIManager>();
        PressF.gameObject.SetActive(false);
        RocketGameobject = GetComponent<GameObject>();
        
    }

    // Update is called once per frame
    void Update()
    {
        RocketControll();
        
    }


    private void RocketControll()
    {
        float Distance = Vector3.Distance(InteraktionPoint.position, Player.transform.position);
        if (GizmosRadius >= Distance)
        {
            PressF.gameObject.SetActive(true);

            if (Input.GetKey(KeyCode.F))
            {
                pressButton = true;
                PressF.gameObject.SetActive(false);
            }            
        }
        
        if (uIManager.PowerChargeRocket >= 100)
        {
            cooldown -= Time.deltaTime;
            RocketRb.AddForce(-RocketForce, 0, 0);
            Destroy(RocketGameobject, 4f);
            Destroy(this, 4f);

            if (cooldown <= 0)
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, 20);
                foreach (Collider hit in colliders)
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();

                    if (rb != null)
                        rb.AddExplosionForce(200, transform.position, 20);
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(GizmosCenter.position, GizmosRadius);
    }
}
