﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThePortalB : MonoBehaviour
{
    [SerializeField]
    private Transform PlayerCamera;
    [SerializeField]
    private Transform PortalA;
    [SerializeField]
    private Transform PortalB;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FollowCamera();
    }
    //Nochmals angucken
    private void FollowCamera()
    {
        //Playerposition = PlayerCamera.position + Coordinates for the right Portal vision;
        Vector3 PlayerCameraOffset = new Vector3(-6, 0, 5);
        Vector3 playerOffsetFromPortal = (PlayerCamera.position + PlayerCameraOffset) - PortalA.position;
        transform.position = PortalB.position + playerOffsetFromPortal;

        float angularDifferenceBetweenPortalsRotation = Quaternion.Angle(PortalB.rotation, PortalA.rotation);
        Quaternion portalRotationDifference = Quaternion.AngleAxis(angularDifferenceBetweenPortalsRotation, Vector3.up);
        Quaternion CameraY = Quaternion.Euler(0, -80, 0);
        Vector3 newCameraDirection = portalRotationDifference * PlayerCamera.forward;
        transform.rotation = Quaternion.LookRotation(-newCameraDirection + new Vector3(0,CameraY.y,0), Vector3.up);
    }
}
