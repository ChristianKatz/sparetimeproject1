﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTextureSetup : MonoBehaviour
{
    [SerializeField]
    private Camera CameraB;
    [SerializeField]
    private Material CameraMaterialB;
    [SerializeField]
    private Camera CameraA;
    [SerializeField]
    private Material CameraMaterialA;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(CameraB.targetTexture != null)
        {
            CameraB.targetTexture.Release();
        }
        CameraB.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        CameraMaterialB.mainTexture = CameraB.targetTexture;

        if (CameraA.targetTexture != null)
        {
            CameraA.targetTexture.Release();
        }
        CameraA.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        CameraMaterialA.mainTexture = CameraA.targetTexture;

    }
}
