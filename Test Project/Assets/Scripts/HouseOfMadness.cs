﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HouseOfMadness : MonoBehaviour
{   
    [SerializeField]
    private Animator[] animator;
    [SerializeField]
    private Transform InteractionPosition;
    [SerializeField]
    private float Radius;
    [SerializeField]
    private Transform Player;
    [SerializeField]
    private TextMeshProUGUI PressF;

    private bool OpenTheGate = false;

    void Start()
    {
        PressF.gameObject.SetActive(false);
    }

    void Update()
    {
        PressButton();
    }
    void PressButton()
    {
        float Distance = Vector3.Distance(InteractionPosition.position, Player.position);
        if (Distance <= Radius)
        {
            PressF.gameObject.SetActive(true);

            if(Input.GetKeyDown(KeyCode.F))
            {
                animator[0].SetBool("PressButton", true);
                animator[1].SetTrigger("TextAppear");
                PressF.gameObject.SetActive(false);
                StartCoroutine(openTheGate());
            }
            if (Input.GetKeyDown(KeyCode.F) && OpenTheGate == true)
            {              
                animator[2].SetTrigger("OpenGate");
                animator[0].SetBool("PressButton", true);
                PressF.gameObject.SetActive(false);
               
            }
        }
        else
        {
            PressF.gameObject.SetActive(false);
        }
    }
    IEnumerator openTheGate()
    {        
        yield return new WaitForSeconds(5f);
        OpenTheGate = true;
        PressF.gameObject.SetActive(true);
        animator[0].SetBool("PressButton", false);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(InteractionPosition.position, Radius);
    }

}
