﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using TMPro;

public class ShopWindow : MonoBehaviour
{

    [SerializeField]
    private PlayableDirector OpenShopWindow;
    [SerializeField]
    private PlayableDirector CloseShopWindow;
    private bool WindowIsOpened = false;
    private bool WindowIsClosed = true;

    [SerializeField]
    private GameObject Canvas;

    [SerializeField]
    private Transform GizmosCenter;
    [SerializeField]
    private float GizmosRadius;

    [SerializeField]
    private GameObject Player;

    [SerializeField]
    private TextMeshProUGUI PressF;
    // Start is called before the first frame update
    void Start()
    {
        PressF.gameObject.SetActive(false);
        Canvas.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        ShopWindowControll();
    }
    private void ShopWindowControll()
    {
        float Distance = Vector3.Distance(GizmosCenter.position, Player.transform.position);
            
        if(GizmosRadius >= Distance)
        {
            PressF.gameObject.SetActive(true);

            if (Input.GetKey(KeyCode.F) && WindowIsClosed == true)
            {
                Canvas.gameObject.SetActive(true);
                OpenShopWindow.Play();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                WindowIsClosed = false;
                StartCoroutine(openedWindow());
            }
            if(Input.GetKey(KeyCode.F) && WindowIsOpened == true)
            {
                OpenShopWindow.Stop();
                CloseShopWindow.Play();
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;               
                WindowIsOpened = false;
                StartCoroutine(closedWindow());
            }
        } 
        else
        {
            PressF.gameObject.SetActive(false);
        }
    }
    IEnumerator openedWindow()
    {
        yield return new WaitForSeconds(3f);
        WindowIsOpened = true;
    }
    IEnumerator closedWindow()
    {
        yield return new WaitForSeconds(3f);
        WindowIsClosed = true;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(GizmosCenter.position, GizmosRadius);
    }
}
