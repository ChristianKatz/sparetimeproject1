﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayer : MonoBehaviour
{
    public int Health;
    public int Armor;
    public int Damage;

    private Rigidbody rb;

    [SerializeField]
    private int speed;

    [SerializeField]
    private int jumpforce = 500;
    public bool isGrounded;
    private Transform jumpLocation;
    private float maxJumpDistance = 0.1f;
    private LayerMask groundLayer;
    private Transform PlayerTransform;
    private float XRotation;

    [SerializeField]
    private int playerRotateSpeed;
    
    [SerializeField]
    private float gunDamage;
    [SerializeField]
    private float fireRate;
    [SerializeField]
    private float fireRange;
    [SerializeField]
    private float hitForce;
    [SerializeField]
    private Transform ShootPosition;
    private WaitForSeconds fireDuration = new WaitForSeconds(0.2f);
    [SerializeField]
    private AudioSource gunSound;
    private LineRenderer laser;
    private float nextGunFire = 5;
    private bool gunFired;
    public bool GunFired
    {
        get
        {
            return gunFired;
        }
        set
        {
            gunFired = value;
        }

    }
    [SerializeField]
    private Transform Gun;
    private float Clamp;

    private int maxPower = 4000;
    [SerializeField]
    private GameObject Granate;
    [SerializeField]
    private Transform GranateReleaseposition;
    private float shootPower;
    public float ShootPower
    {
        get
        {
            return shootPower;
        }
        set
        {
            shootPower = value;
        }
    }
    private float nextGrenateFire;
    private float GranateFireRate = 5;
    [SerializeField]
    private Animator animator;
    private bool granateFired;
    public bool GranateFired
    {
        get
        {
            return granateFired;
        }
        set
        {
           granateFired = value;
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        PlayerTransform = GetComponent<Transform>();
        jumpLocation = gameObject.transform.Find("Jumping");
        isGrounded = false;
        groundLayer.value = 1;       
        laser = GetComponent<LineRenderer>();
        laser.enabled = false;
        GranateFired = false;
        Cursor.lockState = CursorLockMode.Locked;
        gunFired = false;
        
    }

    void Update()
    {     
        shootingGun();
        RotatePlayer();
        Mortar();
        WeaponMovementControll();
        MeeleAttack();       
    }

    private void FixedUpdate()
    {
        Movement();         
    }

    private void Movement()
    {          
        float moveHorizontal = Input.GetAxis("Horizontal") * speed * Time.fixedDeltaTime;
        float moveVertical = Input.GetAxis("Vertical") * speed * Time.fixedDeltaTime;
        rb.MovePosition(transform.position + transform.TransformDirection(moveHorizontal, 0, moveVertical));

        isGrounded = Physics.Raycast(jumpLocation.position, jumpLocation.TransformDirection(Vector3.down), maxJumpDistance, groundLayer);
        if (isGrounded == true)
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rb.AddForce(new Vector3(0, jumpforce, 0));                         
            }         
    }

    private void RotatePlayer()
    {
        float turnAround = Input.GetAxis("Mouse X") * playerRotateSpeed;
        XRotation += turnAround;
        Quaternion PlayerRotation = Quaternion.Euler(0, XRotation, 0);
        rb.MoveRotation(PlayerRotation);       
    }

    private void shootingGun()
    {              
        if (Input.GetButtonDown("Fire1") && Time.time > nextGunFire)
        {
            nextGunFire = Time.time + fireRate;
            StartCoroutine(ShootEffect());
            laser.SetPosition(0,ShootPosition.position);

            RaycastHit hit;
            if(Physics.Raycast(ShootPosition.position, ShootPosition.transform.forward, out hit, fireRange))
            {
                laser.SetPosition(1, hit.point);
                gunFired = true;
            }
            else
            {
                laser.SetPosition(1, ShootPosition.position + (ShootPosition.transform.forward * fireRange));
                gunFired = true;
            }
            if(hit.rigidbody != null )
            {
                hit.rigidbody.AddForce(-hit.normal * hitForce);
            }
            IEnumerator ShootEffect()
            {
            gunSound.Play();
            laser.enabled = true;
            yield return fireDuration;
            gunFired = false;
            laser.enabled = false;
            }             
        }     
    }  

    private void WeaponMovementControll()
    {              
        float MouseY = Input.GetAxis("Mouse Y");
        Clamp += MouseY;

        if(Clamp >= 30)
        {
            Clamp = 30;
            MouseY = 0;
        }
        if(Clamp <= -30)
        {
            Clamp = -30;
            MouseY = 0;
        }
        Gun.transform.Rotate( 0, 0, MouseY);
    }

    private void Mortar()
    {
       if(Input.GetMouseButton(1) && Time.time > nextGrenateFire)
        {
           
            if( shootPower < maxPower)
            {
                shootPower += Time.deltaTime * 1000;
                
            }
            if(shootPower >= maxPower)
            {
                shootPower = 4000;               
            }
        }
       if(Input.GetMouseButtonUp(1) && Time.time > nextGrenateFire)
       {
            GranateFired = true;
            GameObject GranateInstance = Instantiate(Granate, GranateReleaseposition.position, GranateReleaseposition.rotation);
            GranateInstance.GetComponent<Rigidbody>().AddForce(transform.forward * shootPower);
            Destroy(GranateInstance, 5);
            nextGrenateFire = Time.time + GranateFireRate;
            shootPower = 0;
            
       }
    }

    public void MeeleAttack()
    {
        if (Input.GetKey(KeyCode.V))
        {
            animator.SetBool("MeleeAttack", true);
        }
        else
        {
            animator.SetBool("MeleeAttack", false);
        }
    }
}

