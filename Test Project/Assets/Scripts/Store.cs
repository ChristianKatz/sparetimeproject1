﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Store : MonoBehaviour
{
    [SerializeField]
    private PlayableDirector timelineToGoIn;

    [SerializeField]
    private PlayableDirector CameraInTheHouse;

    [SerializeField]
    private PlayableDirector CameraOutTheHouse;

    [SerializeField]
    private GameObject Player;

    [SerializeField]
    private GameObject Something;

    private bool GoIn = false;
    private bool GoOut = false;

    [SerializeField]
    private Transform targetPosition;

   
    [SerializeField]
    private Transform GizmosCenter;
    [SerializeField]
    private float GizmosRadius;
    private BoxCollider Collider;
    [SerializeField]
    private Transform targetPosition2;
    private bool GizmosIsActivated = false;

    void Start()
    {
        Collider = GetComponent<BoxCollider>();
        
    }

    void Update()
    {
        GoInTheShop();
        
        GoOutTheShop();
    }

    private void GoInTheShop()
    {
        if (GoIn == true)
        {
            Vector3 PlayerInTheShop = Vector3.MoveTowards(Player.transform.position, targetPosition.position, 10 * Time.deltaTime);
            Player.transform.position = PlayerInTheShop;

            if (Player.transform.position == targetPosition.position)
            {
                GoIn = false;
                GoOut = true;
            }

        }
    }

    private void GoOutTheShop()
    {

        if (GoOut == true)
        {
            float Distance = Vector3.Distance(GizmosCenter.position, Something.transform.position);

            if (GizmosRadius >= Distance)
            {
                timelineToGoIn.Play();
                CameraInTheHouse.Stop();
                CameraOutTheHouse.Play();
                Collider.enabled = false;
                StartCoroutine(WaitToGoOut());
            
            }
            if(GizmosIsActivated == true)
            {
                Collider.enabled = false;

                Vector3 PlayerOutTheShop = Vector3.MoveTowards(Player.transform.position, targetPosition2.position, 10 * Time.deltaTime);
                Player.transform.position = PlayerOutTheShop;
                
                if (Player.transform.position == targetPosition2.position)
                {
                    GoOut = false;
                    GizmosIsActivated = false;
                    Collider.enabled = true;
                    
                }                                                   
            }
       }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            timelineToGoIn.Play();
            CameraInTheHouse.Play();
            StartCoroutine(WaitToGoIn());
        }
        
    }
    IEnumerator WaitToGoIn()
    {
       yield return new WaitForSeconds(1f);
       GoIn = true;
    }
    IEnumerator WaitToGoOut()
    {
        yield return new WaitForSeconds(1f);
        GizmosIsActivated = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(GizmosCenter.position, GizmosRadius);
    }
}

